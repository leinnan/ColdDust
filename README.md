# ColdDust
Make sure to clone this repository with the `--recursive` flag

## Build

```
mkdir build
cd build
cmake ..
make
```
